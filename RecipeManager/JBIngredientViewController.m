//
//  JBViewController.m
//  RecipeManager
//
//  Created by James Borden on 9/13/14.
//  Copyright (c) 2014 James Borden. All rights reserved.
//

#import "JBIngredientViewController.h"
#import "JBAddIngredientViewController.h"
#import "CouchbaseLite.h"

@implementation JBIngredientViewController

- (NSString *)segueIdentifier {
    return @"addIngredient";
}

- (NSString *)queryViewName {
    return @"ingredients";
}

@end
