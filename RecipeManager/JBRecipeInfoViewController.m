//
//  JBRecipeInfoViewController.m
//  RecipeManager
//
//  Created by James Borden on 10/4/14.
//  Copyright (c) 2014 James Borden. All rights reserved.
//

#import "JBRecipeInfoViewController.h"
#import "Couchbaselite.h"
#import "JBUnits.h"

@interface JBRecipeInfoViewController () <UITableViewDataSource, UITabBarDelegate>

@property (weak, nonatomic) IBOutlet UITableView *infoTableView;

@end

@implementation JBRecipeInfoViewController
{
    NSMutableDictionary *mDataSource;
    NSArray *mKeyIndex;
}

- (void)loadRecipe:(CBLDocument *)recipe {
    mDataSource = [NSMutableDictionary new];
    CBLDatabase *db = [[CBLManager sharedInstance] databaseNamed:@"recipemanager" error:nil];
    
    NSDictionary *ingredients = recipe.userProperties[@"tableProps"];
    CBLView *ingredientView = [db viewNamed:@"ingredients"];
    CBLQuery *query = [ingredientView createQuery];
    [query setKeys:[ingredients allKeys]];
    [query runAsync:^(CBLQueryEnumerator *item, NSError *err) {
        for (CBLQueryRow *ingredientName in item) {
            CBLDocument *ingredient = [ingredientName document];
            JBUnit *amount = [JBUnit newUnitWithString:ingredients[ingredientName.key]];
            [self processIngredient:ingredient amount:amount];
        }
        
        mKeyIndex = [mDataSource allKeys];
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [self.infoTableView reloadData];
        }];
    }];
    
}

- (void)processIngredient:(CBLDocument *)ingredient amount:(JBUnit *)amount {
    NSDictionary *stats = ingredient.userProperties[@"tableProps"];
    JBUnit *servingSize = [JBUnit newUnitWithString:ingredient.userProperties[@"servingSize"]];
    double servings = [amount dividedBy:servingSize];
    for (NSString *statName in stats) {
        JBUnit *statAmount = [JBUnit newUnitWithString:stats[statName]];
        [statAmount multiply:servings];
        JBUnit *existingAmount = mDataSource[statName];
        if(!existingAmount)
            existingAmount = statAmount;
        else
            [existingAmount add:statAmount];
        
        mDataSource[statName] = existingAmount;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return mDataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"InfoCell"];
    if(!cell)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"InfoCell"];
    
    NSString *title = mKeyIndex[indexPath.row];
    cell.textLabel.text = title;
    JBUnit *existing = mDataSource[title];
    JBUnit *amount = nil;
    if([existing isKindOfClass:[JBGeneric class]]) {
        amount = existing;
    } else {
        amount = [[JBGram alloc] initWithUnit:mDataSource[title]];
    }
    
    cell.detailTextLabel.text = [amount description];
    
    return cell;
}

@end
