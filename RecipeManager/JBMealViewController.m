//
//  JBMealViewController.m
//  RecipeManager
//
//  Created by James Borden on 11/15/14.
//  Copyright (c) 2014 James Borden. All rights reserved.
//

#import "JBMealViewController.h"
#import "JBMealInfoViewController.h"

@interface JBMealViewController ()

@end

@implementation JBMealViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (NSString *)segueIdentifier {
    return @"addMeal";
}

- (NSString *)queryViewName {
    return @"meals";
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
    mSelectedItem = mDataSource[indexPath.row];
    [self performSegueWithIdentifier:@"showMealInfo" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"showMealInfo"]) {
        JBMealInfoViewController *next = segue.destinationViewController;
        [next loadMeal:mSelectedItem];
    } else {
        [super prepareForSegue:segue sender:sender];
    }
}

@end
