//
//  JBFunctional.m
//  RecipeManager
//
//  Created by James Borden on 9/21/14.
//  Copyright (c) 2014 James Borden. All rights reserved.
//

#import "JBFunctional.h"

@implementation NSDictionary (Functional)

- (NSArray *)linearize:(linearize_function_t)func {
    if(!func)
        return nil;
    
    NSMutableArray *retVal = [NSMutableArray new];
    [self enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
        id next = func(key, obj);
        if(next)
            [retVal addObject:next];
    }];
    
    return [retVal copy];
}

@end

@implementation JBFunctional



@end
