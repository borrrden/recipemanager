//
//  JBFunctional.h
//  RecipeManager
//
//  Created by James Borden on 9/21/14.
//  Copyright (c) 2014 James Borden. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef id(^linearize_function_t)(id<NSCopying> key, id value);

@interface NSDictionary (Functional)

- (NSArray *)linearize:(linearize_function_t)func;

@end

@interface JBFunctional : NSObject

@end
