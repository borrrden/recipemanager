//
//  JBAppDelegate.m
//  RecipeManager
//
//  Created by James Borden on 9/13/14.
//  Copyright (c) 2014 James Borden. All rights reserved.
//

#import "JBAppDelegate.h"
#import "CouchbaseLite.h"
#import "JBUnits.h"

@implementation JBAppDelegate
{
    CBLReplication *mPush;
    CBLReplication *mPull;
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    CBLManager *manager = [CBLManager sharedInstance];
    CBLDatabase *db = [manager databaseNamed:@"recipemanager" error:nil];
    CBLView *ingredientView = [db viewNamed:@"ingredients"];
    [ingredientView setMapBlock:^(NSDictionary *doc, CBLMapEmitBlock emit) {
        if([doc[@"type"] isEqualToString:@"ingredient"])
            emit(doc[@"name"], nil);
    } version:@"2"];
    
    CBLView *recipeView = [db viewNamed:@"recipes"];
    [recipeView setMapBlock:^(NSDictionary *doc, CBLMapEmitBlock emit) {
        if([doc[@"type"] isEqualToString:@"recipe"])
            emit(doc[@"name"], nil);
    } version:@"2"];
    
    CBLView *mealView = [db viewNamed:@"meals"];
    [mealView setMapBlock:^(NSDictionary *doc, CBLMapEmitBlock emit) {
        if([doc[@"type"] isEqualToString:@"meal"])
            emit(doc[@"name"], nil);
    } version:@"2"];
    
    NSURL *syncUrl = [NSURL URLWithString:@"http://127.0.0.1:4984/sync_gateway"];
    mPull = [db createPullReplication:syncUrl];
    mPull.continuous = YES;
    mPush = [db createPushReplication:syncUrl];
    mPush.continuous = YES;
    
    [mPull start];
    [mPush start];
    
    return YES;
}

@end
