//
//  JBAddViewController.m
//  RecipeManager
//
//  Created by James Borden on 10/4/14.
//  Copyright (c) 2014 James Borden. All rights reserved.
//

#import "JBAddViewController.h"
#import "JBIngredientDataCell.h"
#import "JBFunctional.h"
#import "CouchbaseLite.h"
#import <ReactiveCocoa/ReactiveCocoa.h>

@interface JBAddViewController ()

- (IBAction)addField;

@end

@implementation JBAddViewController

@dynamic itemType;

- (void)loadExistingItem:(CBLDocument *)item {
    mDoc = item;
    
    mTitle = mDoc.userProperties[@"name"];
    mDataSource = [[mDoc.userProperties[@"tableProps"] linearize:^id(NSString *key, NSString *value) {
        return [@[key, value] mutableCopy];
    }] mutableCopy];
    
    [self.dataTable reloadData];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.edgesForExtendedLayout = UIRectEdgeNone;
    
    if(!mDataSource)
        mDataSource = [NSMutableArray new];
    
    self.nameField.text = mTitle;
    if(!mDoc) {
         CBLDatabase *db = [[CBLManager sharedInstance] databaseNamed:@"recipemanager" error:nil];
        mDoc = [db createDocument];
        [mDoc putProperties:@{@"type": self.itemType} error:nil];
    }
}

- (void)performSave:(CBLUnsavedRevision *)document {
    //Virtual
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    
    NSMutableDictionary *tableProps = [NSMutableDictionary new];
    for(NSArray *tuple in mDataSource) {
        tableProps[tuple[0]] = tuple[1];
    }
    
    [mDoc update:^BOOL(CBLUnsavedRevision *rev) {
        rev[@"name"] = self.nameField.text;
        rev[@"tableProps"] = tableProps;
        [self performSave:rev];
        return YES;
    } error:nil];
}

- (IBAction)addField {
    [mDataSource addObject:[@[@"", @""] mutableCopy]];
    [self.dataTable reloadData];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [mDataSource count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSAssert(NO, @"Override me");
    
    return nil;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSUInteger row = [indexPath row];
    [mDataSource removeObjectAtIndex:row];
    
    [self.dataTable deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    
}

@end
