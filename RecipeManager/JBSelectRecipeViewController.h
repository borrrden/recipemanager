//
//  JBSelectRecipeViewController.h
//  RecipeManager
//
//  Created by James Borden on 11/15/14.
//  Copyright (c) 2014 James Borden. All rights reserved.
//

#import "JBLookupViewController.h"

@protocol JBSelectRecipeDelegate <NSObject>

- (void)selectedRecipe:(NSString *)recipe;

@end

@interface JBSelectRecipeViewController : JBLookupViewController

@property (nonatomic, weak) id<JBSelectRecipeDelegate> delegate;

@end
