//
//  JBUnits.m
//  RecipeManager
//
//  Created by James Borden on 10/4/14.
//  Copyright (c) 2014 James Borden. All rights reserved.
//

#import "JBUnits.h"

static NSDictionary *ABBREVIATION_MAP = nil;

@implementation JBUnit

- (instancetype)initWithAmount:(double)amount {
    self = [super init];
    if(self) {
        _amount = amount;
    }
    
    return self;
}

- (instancetype)initWithUnit:(JBUnit *)unit {
    if([self isMemberOfClass:[unit class]]) {
        return [self initWithAmount:unit.amount];
    }
    
    double amount = [unit amountInGrams] / [self amountPerGram];
    
    return [self initWithAmount:amount];
}

- (double)amountPerGram {
    NSAssert(NO, @"Method must be overriden");
    
    return 0.0;
}

- (double)amountInGrams {
    return self.amount / [self amountPerGram];
}

- (void)add:(JBUnit *)unit {
    JBUnit *other = [[[self class] alloc] initWithUnit:unit];
    self.amount += other.amount;
}

- (void)multiply:(double)amount {
    self.amount *= amount;
}

- (double)dividedBy:(JBUnit *)amount {
    return [self amountInGrams] / [amount amountInGrams];
}

+ (JBUnit *)newUnitWithString:(NSString *)unitString {
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        ABBREVIATION_MAP = @{@"g": [JBGram class], @"kg": [JBKilogram class], @"mg": [JBMilligram class], @"lb": [JBPound class], @"oz": [JBOunce class]};
    });
    
    NSScanner *scanner = [[NSScanner alloc] initWithString:unitString];
    double amount;
    if(![scanner scanDouble:&amount])
        return nil;

    if([scanner isAtEnd])
        return [[JBGeneric alloc] initWithAmount:amount];
        
    NSString *unit = [unitString substringFromIndex:[scanner scanLocation]];
    Class unitClass = ABBREVIATION_MAP[unit];
    if(!unitClass)
        return nil;
    
    return [[unitClass alloc] initWithAmount:amount];
}

@end

@implementation JBGram

- (double)amountPerGram {
    return 1.0;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"%gg", self.amount];
}

@end

@implementation JBKilogram

- (double)amountPerGram {
    return 0.001;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"%gkg", self.amount];
}

@end

@implementation JBMilligram

- (double)amountPerGram {
    return 1000;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"%gmg", self.amount];
}

@end

@implementation JBPound

- (double)amountPerGram {
    return 0.00220462;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"%glb", self.amount];
}

@end

@implementation JBOunce

- (double)amountPerGram {
    return 0.035274;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"%goz", self.amount];
}

@end

@implementation JBGeneric

- (double)amountPerGram {
    return 1;
}

- (NSString *)description {
    return [NSString stringWithFormat:@"%g", self.amount];
}

@end
                
    
