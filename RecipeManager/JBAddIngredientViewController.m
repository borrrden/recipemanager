//
//  JBAddIngredientViewController.m
//  RecipeManager
//
//  Created by James Borden on 9/13/14.
//  Copyright (c) 2014 James Borden. All rights reserved.
//

#import "JBAddIngredientViewController.h"
#import "JBIngredientDataCell.h"
#import "JBFunctional.h"
#import "CouchbaseLite.h"
#import <ReactiveCocoa/ReactiveCocoa.h>

@interface JBAddIngredientViewController ()

@property (weak, nonatomic) IBOutlet UITextField *servingSizeField;

@end

@implementation JBAddIngredientViewController

- (NSString *)itemType {
    return @"ingredient";
}

- (void)performSave:(CBLUnsavedRevision *)document {
    document[@"servingSize"] = self.servingSizeField.text.length > 0 ? self.servingSizeField.text : @"100g";
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.dataTable registerNib:[UINib nibWithNibName:@"JBIngredientDataCell" bundle:nil] forCellReuseIdentifier:@"JBIngredientDataCell"];
    self.servingSizeField.text = mDoc.userProperties[@"servingSize"];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    JBIngredientDataCell *cell =  [tableView dequeueReusableCellWithIdentifier:@"JBIngredientDataCell" forIndexPath:indexPath];
    
    cell.nameField.text = mDataSource[indexPath.row][0];
    cell.amountField.text = mDataSource[indexPath.row][1];
    
    [cell.nameField.rac_textSignal subscribeNext:^(NSString *newVal) {
        mDataSource[indexPath.row][0] = newVal;
    }];
    
    [cell.amountField.rac_textSignal subscribeNext:^(NSString *newVal) {
        mDataSource[indexPath.row][1] = newVal;
    }];
    
    return cell;
}

@end
