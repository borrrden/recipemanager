//
//  JBRecipeViewController.m
//  RecipeManager
//
//  Created by James Borden on 10/4/14.
//  Copyright (c) 2014 James Borden. All rights reserved.
//

#import "JBRecipeViewController.h"
#import "JBRecipeInfoViewController.h"

@implementation JBRecipeViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

- (NSString *)segueIdentifier {
    return @"addRecipe";
}

- (NSString *)queryViewName {
    return @"recipes";
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [super tableView:tableView cellForRowAtIndexPath:indexPath];
    cell.accessoryType = UITableViewCellAccessoryDetailDisclosureButton;
    
    return cell;
}

- (void)tableView:(UITableView *)tableView accessoryButtonTappedForRowWithIndexPath:(NSIndexPath *)indexPath {
    mSelectedItem = mDataSource[indexPath.row];
    [self performSegueWithIdentifier:@"showRecipeInfo" sender:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if([segue.identifier isEqualToString:@"showRecipeInfo"]) {
        JBRecipeInfoViewController *next = segue.destinationViewController;
        [next loadRecipe:mSelectedItem];
    } else {
        [super prepareForSegue:segue sender:sender];
    }
}

@end
