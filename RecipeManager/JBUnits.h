//
//  JBUnits.h
//  RecipeManager
//
//  Created by James Borden on 10/4/14.
//  Copyright (c) 2014 James Borden. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface JBUnit : NSObject

@property (nonatomic) double amount;

- (instancetype)initWithUnit:(JBUnit *)unit;

- (instancetype)initWithAmount:(double)amount;

- (double)amountPerGram;

- (double)amountInGrams;

- (void)add:(JBUnit *)unit;

- (void)multiply:(double)amount;

- (double)dividedBy:(JBUnit *)amount;

+ (JBUnit *)newUnitWithString:(NSString *)unitString;

@end

@interface JBGram : JBUnit

@end

@interface JBKilogram : JBUnit

@end

@interface JBMilligram : JBUnit

@end

@interface JBPound : JBUnit

@end

@interface JBOunce : JBUnit

@end

@interface JBGeneric : JBUnit

@end