//
//  JBAddMealViewController.m
//  RecipeManager
//
//  Created by James Borden on 11/15/14.
//  Copyright (c) 2014 James Borden. All rights reserved.
//

#import "JBAddMealViewController.h"
#import "JBSelectRecipeViewController.h"
#import "JBRecipeDataCell.h"
#import <ReactiveCocoa/ReactiveCocoa.h>

@interface JBAddMealViewController () <JBSelectRecipeDelegate>

@end

@implementation JBAddMealViewController

- (NSString *)itemType {
    return @"meal";
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.dataTable registerNib:[UINib nibWithNibName:@"JBRecipeDataCell" bundle:nil] forCellReuseIdentifier:@"JBRecipeDataCell"];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    JBRecipeDataCell *cell =  [tableView dequeueReusableCellWithIdentifier:@"JBRecipeDataCell" forIndexPath:indexPath];
    
    cell.nameLabel.text = mDataSource[indexPath.row][0];
    cell.amountField.hidden = YES;
    
    return cell;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    JBSelectRecipeViewController *dest = segue.destinationViewController;
    dest.delegate = self;
}

- (void)selectedRecipe:(NSString *)recipe {
    [mDataSource addObject:@[recipe, @""]];
    [self.dataTable reloadData];
}

@end
