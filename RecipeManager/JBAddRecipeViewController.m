//
//  JBAddRecipeViewController.m
//  RecipeManager
//
//  Created by James Borden on 10/4/14.
//  Copyright (c) 2014 James Borden. All rights reserved.
//

#import "JBAddRecipeViewController.h"
#import "JBSelectIngredientViewController.h"
#import "JBRecipeDataCell.h"
#import <ReactiveCocoa/ReactiveCocoa.h>

@interface JBAddRecipeViewController () <JBSelectIngredientDelegate>

@end

@implementation JBAddRecipeViewController

- (NSString *)itemType {
    return @"recipe";
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.dataTable registerNib:[UINib nibWithNibName:@"JBRecipeDataCell" bundle:nil] forCellReuseIdentifier:@"JBRecipeDataCell"];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    JBRecipeDataCell *cell =  [tableView dequeueReusableCellWithIdentifier:@"JBRecipeDataCell" forIndexPath:indexPath];
    
    cell.nameLabel.text = mDataSource[indexPath.row][0];
    cell.amountField.text = mDataSource[indexPath.row][1];
    
    [cell.amountField.rac_textSignal subscribeNext:^(NSString *newVal) {
        mDataSource[indexPath.row][1] = newVal;
    }];
    
    return cell;
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    JBSelectIngredientViewController *dest = segue.destinationViewController;
    dest.delegate = self;
}

- (void)selectedIngredient:(NSString *)ingredient {
    [mDataSource addObject:[@[ingredient, @""] mutableCopy]];
    [self.dataTable reloadData];
}

@end
