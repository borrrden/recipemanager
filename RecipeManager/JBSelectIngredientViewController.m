//
//  JBSelectIngredientViewController.m
//  RecipeManager
//
//  Created by James Borden on 10/4/14.
//  Copyright (c) 2014 James Borden. All rights reserved.
//

#import "JBSelectIngredientViewController.h"
#import "CouchbaseLite.h"

@interface JBSelectIngredientViewController () <UISearchBarDelegate>

@end

@implementation JBSelectIngredientViewController

- (NSString *)segueIdentifier {
    return @"selectIngredient";
}

- (NSString *)queryViewName {
    return @"ingredients";
}

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    CBLDatabase *db = [[CBLManager sharedInstance] databaseNamed:@"recipemanager" error:nil];
    CBLView *view = [db viewNamed:self.queryViewName];
    CBLQuery *query = [view createQuery];
    query.startKey = searchText;
    query.endKey = [searchText stringByAppendingString:@"z"];
    
    [query runAsync:^(CBLQueryEnumerator *results, NSError *err) {
        NSMutableArray *arr = [[NSMutableArray alloc] initWithCapacity:results.count];
        for(int i = 0; i < results.count; i++) {
            CBLQueryRow *row = [results nextRow];
            [arr addObject:[row key]];
        }
        
        mDataSource = arr;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.lookupTable reloadData];
        });
    }];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSString *selectedItem = [mDataSource[indexPath.row] userProperties][@"name"];
    [self.delegate selectedIngredient:selectedItem];
    [self.navigationController popViewControllerAnimated:YES];
}

@end
