//
//  JBSelectIngredientViewController.h
//  RecipeManager
//
//  Created by James Borden on 10/4/14.
//  Copyright (c) 2014 James Borden. All rights reserved.
//

#import "JBLookupViewController.h"

@protocol JBSelectIngredientDelegate <NSObject>

- (void)selectedIngredient:(NSString *)ingredient;

@end

@interface JBSelectIngredientViewController : JBLookupViewController

@property (nonatomic, weak) id<JBSelectIngredientDelegate> delegate;

@end
