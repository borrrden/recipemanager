//
//  JBIngredientDataCell.h
//  RecipeManager
//
//  Created by James Borden on 9/13/14.
//  Copyright (c) 2014 James Borden. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JBIngredientDataCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UITextField *amountField;

@end
