//
//  JBMealInfoViewController.h
//  RecipeManager
//
//  Created by James Borden on 11/15/14.
//  Copyright (c) 2014 James Borden. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CBLDocument;

@interface JBMealInfoViewController : UIViewController

- (void)loadMeal:(CBLDocument *)mealName;

@end
