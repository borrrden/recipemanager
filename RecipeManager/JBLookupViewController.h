//
//  JBLookupAddViewController.h
//  RecipeManager
//
//  Created by James Borden on 10/4/14.
//  Copyright (c) 2014 James Borden. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CBLDocument;

@interface JBLookupViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
{
    @protected
    NSMutableArray *mDataSource;
    CBLDocument *mSelectedItem;
}

@property (weak, nonatomic) IBOutlet UITableView *lookupTable;

@property (nonatomic, readonly) NSString *queryViewName;

@property (nonatomic, readonly) NSString *segueIdentifier;

@end
