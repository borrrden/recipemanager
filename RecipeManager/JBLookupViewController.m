//
//  JBLookupAddViewController.m
//  RecipeManager
//
//  Created by James Borden on 10/4/14.
//  Copyright (c) 2014 James Borden. All rights reserved.
//

#import "JBLookupViewController.h"
#import "JBAddViewController.h"
#import "CouchbaseLite.h"

@implementation JBLookupViewController

@dynamic queryViewName, segueIdentifier;

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    CBLDatabase *db = [[CBLManager sharedInstance] databaseNamed:@"recipemanager" error:nil];
    CBLView *view = [db viewNamed:self.queryViewName];
    CBLQuery *query = [view createQuery];
    
    [query runAsync:^(CBLQueryEnumerator *results, NSError *err) {
        NSMutableArray *arr = [[NSMutableArray alloc] initWithCapacity:results.count];
        for(int i = 0; i < results.count; i++) {
            CBLQueryRow *row = [results nextRow];
            [arr addObject:row.document];
        }
        
        mDataSource = arr;
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.lookupTable reloadData];
        });
    }];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return mDataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"lookupCell"];
    if(!cell) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"lookupCell"];
    }
    
    cell.textLabel.text = [mDataSource[indexPath.row] userProperties][@"name"];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    mSelectedItem = mDataSource[indexPath.row];
    [self performSegueWithIdentifier:self.segueIdentifier sender:self];
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSUInteger row = [indexPath row];
    CBLDocument *doc = mDataSource[row];
    [mDataSource removeObjectAtIndex:row];
    
    [self.lookupTable deleteRowsAtIndexPaths:[NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];

    [doc deleteDocument:nil];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if(!mSelectedItem)
        return;
    
    JBAddViewController *dest = segue.destinationViewController;
    [dest loadExistingItem:mSelectedItem];
    mSelectedItem = nil;
}

@end
