//
//  JBRecipeDataCell.m
//  RecipeManager
//
//  Created by James Borden on 10/4/14.
//  Copyright (c) 2014 James Borden. All rights reserved.
//

#import "JBRecipeDataCell.h"

@implementation JBRecipeDataCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
