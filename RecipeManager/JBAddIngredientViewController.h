//
//  JBAddIngredientViewController.h
//  RecipeManager
//
//  Created by James Borden on 9/13/14.
//  Copyright (c) 2014 James Borden. All rights reserved.
//

#import "JBAddViewController.h"

@interface JBAddIngredientViewController : JBAddViewController

@end
