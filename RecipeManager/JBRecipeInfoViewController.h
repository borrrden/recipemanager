//
//  JBRecipeInfoViewController.h
//  RecipeManager
//
//  Created by James Borden on 10/4/14.
//  Copyright (c) 2014 James Borden. All rights reserved.
//

#import <UIKit/UIKit.h>
@class CBLDocument;

@interface JBRecipeInfoViewController : UIViewController

- (void)loadRecipe:(CBLDocument *)recipe;

@end
