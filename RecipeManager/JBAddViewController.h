//
//  JBAddViewController.h
//  RecipeManager
//
//  Created by James Borden on 10/4/14.
//  Copyright (c) 2014 James Borden. All rights reserved.
//

#import <UIKit/UIKit.h>

@class CBLUnsavedRevision;
@class CBLDocument;

@interface JBAddViewController : UIViewController <UITableViewDataSource, UITableViewDelegate>
{
    @protected
    CBLDocument *mDoc;
    NSMutableArray *mDataSource;
    NSString *mTitle;
}

@property (weak, nonatomic) IBOutlet UITextField *nameField;
@property (weak, nonatomic) IBOutlet UITableView *dataTable;
@property (nonatomic, readonly) NSString *itemType;

- (void)loadExistingItem:(CBLDocument *)item;

- (void)performSave:(CBLUnsavedRevision *)document;

@end
