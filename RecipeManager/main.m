//
//  main.m
//  RecipeManager
//
//  Created by James Borden on 9/13/14.
//  Copyright (c) 2014 James Borden. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "JBAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([JBAppDelegate class]));
    }
}
