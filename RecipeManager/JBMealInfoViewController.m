//
//  JBMealInfoViewController.m
//  RecipeManager
//
//  Created by James Borden on 11/15/14.
//  Copyright (c) 2014 James Borden. All rights reserved.
//

#import "JBMealInfoViewController.h"
#import "CouchbaseLite.h"
#import "JBUnits.h"

@interface JBMealInfoViewController ()

@property (weak, nonatomic) IBOutlet UITableView *infoTableView;

@end

@implementation JBMealInfoViewController
{
    NSMutableDictionary *mDataSource;
    NSArray *mKeyIndex;
}

- (void)loadMeal:(CBLDocument *)meal {
    mDataSource = [NSMutableDictionary new];
    CBLDatabase *db = [[CBLManager sharedInstance] databaseNamed:@"recipemanager" error:nil];

    NSDictionary *recipes = meal.userProperties[@"tableProps"];
    CBLView *recipeView = [db viewNamed:@"recipes"];
    CBLQuery *firstQuery = [recipeView createQuery];
    [firstQuery setKeys:[recipes allKeys]];
    [firstQuery runAsync:^(CBLQueryEnumerator *recipes, NSError *err1) {
        for(CBLQueryRow *recipeRow in recipes) {
            NSDictionary *ingredients = recipeRow.document.userProperties[@"tableProps"];
            CBLView *ingredientView = [db viewNamed:@"ingredients"];
            CBLQuery *query = [ingredientView createQuery];
            [query setKeys:[ingredients allKeys]];
            NSError *err2 = nil;
            CBLQueryEnumerator *item = [query run:&err2];
            for (CBLQueryRow *ingredientName in item) {
                CBLDocument *ingredient = [ingredientName document];
                JBUnit *amount = [JBUnit newUnitWithString:ingredients[ingredientName.key]];
                [self processIngredient:ingredient amount:amount];
            }
        }
        
        mKeyIndex = [mDataSource allKeys];
        [[NSOperationQueue mainQueue] addOperationWithBlock:^{
            [self.infoTableView reloadData];
        }];
    }];
}

- (void)processIngredient:(CBLDocument *)ingredient amount:(JBUnit *)amount {
    NSDictionary *stats = ingredient.userProperties[@"tableProps"];
    JBUnit *servingSize = [JBUnit newUnitWithString:ingredient.userProperties[@"servingSize"]];
    double servings = [amount dividedBy:servingSize];
    for (NSString *statName in stats) {
        JBUnit *statAmount = [JBUnit newUnitWithString:stats[statName]];
        [statAmount multiply:servings];
        JBUnit *existingAmount = mDataSource[statName];
        if(!existingAmount)
            existingAmount = statAmount;
        else
            [existingAmount add:statAmount];
        
        mDataSource[statName] = existingAmount;
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return mDataSource.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"InfoCell"];
    if(!cell)
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle reuseIdentifier:@"InfoCell"];
    
    NSString *title = mKeyIndex[indexPath.row];
    cell.textLabel.text = title;
    JBGram *amount = [[JBGram alloc] initWithUnit:mDataSource[title]];
    cell.detailTextLabel.text = [amount description];
    
    return cell;
}


@end
