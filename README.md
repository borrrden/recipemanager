２０１４年９月のCouchbase Hackthonのために考え出したアプリです。Public Domainに解放するためのライセンスを入れましたので、自由に使ってください。

## Recipe Manager ##

NoSQLにピッタリなデータは何が入るかあんまり予想できないデータです。このアプリは食材やレシピの栄養情報を集めたり、表示したりするアプリです。ユーザーは知っている範囲でその情報を入力し、レシピに食材とその量を指定すれば合計を計算してくれます。

## ソースの取得 ##

```
#!bash
git clone https://bitbucket.org/borrrden/recipemanager.git
cd recipemanager
git submodule update --init --recursive

```